﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace OpenFileOrganizer
{
    class Program
    {
        public static int movedFiles { get; set; } = 0;

        static void Main(string[] args)
        {
            try
            {
               
                ///
                /// Start Reading Directorys 
                ///
                var sourceDirectorys = new directorys();
                var serializer = new XmlSerializer(typeof(directorys));
                var buffer = Encoding.UTF8.GetBytes(File.ReadAllText(Environment.CurrentDirectory + @"\Directorys.xml"));

                ///
                /// Deserialize Directorys.xml to directorys class
                ///
                using (var stream = new MemoryStream(buffer))
                {
                    sourceDirectorys = (directorys)serializer.Deserialize(stream);
                }


                ///
                ///
                ///
                for(int i = 0; i < sourceDirectorys.source.Count(); i++ )
                {
                    if(Directory.Exists(sourceDirectorys.source[i]))
                    {
                        

                        string[] filenames = Directory.GetFiles(sourceDirectorys.source[i],"*.*", SearchOption.TopDirectoryOnly);
                        string[] files = GetFileNames(filenames);
                        Console.WriteLine("Start Organize: " + sourceDirectorys.source[i] + "   (Included "+ files.Length.ToString()+ " files)");

                        for (int y = 0; y < files.Count(); y++)
                        {
                            string firstchar = files[y];
                            string sourcefolder = sourceDirectorys.source[i] + @"\";
                            string destinationfolder = "";

                            if (char.IsNumber(firstchar[0]))
                            {
                                destinationfolder = FinalSlasher(FinalSlasher(sourcefolder) + "0-9");
                                CheckFolder(destinationfolder);
                                MoveFile(sourcefolder, destinationfolder, files[y]); 
                            }
                            else
                            {
                                destinationfolder = FinalSlasher(FinalSlasher(sourcefolder) + firstchar[0]);
                                CheckFolder(destinationfolder);
                                MoveFile(sourcefolder, destinationfolder, files[y]);
                            }
                        }

                        Console.WriteLine("End: " +sourceDirectorys.source[i] + " - is Done!");
                    }
                    else
                    {
                        Console.WriteLine("Directory does not exist: " + sourceDirectorys.source[i]);
                    }
                }

                Console.WriteLine("Completed! " + movedFiles.ToString() + " files moved");

            }
            catch(Exception ex)
            {
                PrintException(ex);
            }

            Console.WriteLine("Press any key to close");
            Console.ReadKey();
        }

        /// <summary>
        ///     Returns Filenames without Path
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private static string[] GetFileNames(string[] files)
        {
            List<String> retval = new List<string>();
            for(int i = 0; i < files.Count(); i++)
            {
                retval.Add(Path.GetFileName(files[i]));
            }

            return retval.ToArray();
        }

        /// <summary>
        ///  Move File 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="filename"></param>
        private static void MoveFile(string source, string destination, string filename)
        {
            if(Directory.Exists(source) && Directory.Exists(destination))
            {
                try
                {
                    File.Move(FinalSlasher(source) + filename, FinalSlasher(destination) + filename);
                    Console.WriteLine(" -- "+ FinalSlasher(source) + filename +" MOVED TO " + FinalSlasher(destination) + filename + " -- ");
                    movedFiles++;
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }

        }

        /// <summary>
        ///  Add slash/Backslash if not exist
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static string FinalSlasher(string source)
        {
            if (source[source.Length - 1] != '\\' && source[source.Length - 1] != '/')
                return source + @"\";
            else
                return source;

        }

        /// <summary>
        ///     Check if Folder exists, If not create folder
        /// </summary>
        /// <param name="folder"></param>
        private static void CheckFolder(string folder)
        {
            if(!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }

        /// <summary>
        ///  Print Exception to Console window
        /// </summary>
        /// <param name="ex"></param>
        private static void PrintException(Exception ex)
        {

            Console.WriteLine("Error: " + ex.Message + Environment.NewLine +
                    "ErrorCode: " + ex.HResult + Environment.NewLine +
                    "StackTrace: " + ex.StackTrace);
            Console.WriteLine("===============================================================================================================");
        }
    }
}

