# Open File Organizer

Is just a short tool to move files in a source directory to sub folder based on their first character
>>>
# Example
1. Source Directory C:\Pictures
2. Included tons of pictures like "Beach_Number.jpg" and "Streets_number.jpg"
3. After Using Open File Organizer all files are located in C:\Pictures\B\Beach_Number.jpg and C:\Pictures\S\Streets_Number.jpg
>>>

# Requirements
1. >= .NET version 4.6 
2. OFO is build with VisualStudio 2015 Community Edition




# Getting Started

1. Compile or download the project
2. Add all directorys to Directorys.xml which you want to sort/organize
```XML
<?xml version="1.0" encoding="utf-8" ?>
<directorys>
  <source>C:\Folder1</source>
  <source>C:\Folder2</source>
  <source>C:\Folder3</source>
</directorys>
```
3. Run OpenFileOrganizer.exe